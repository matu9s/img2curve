# IMG2Curve

Ročníkový Projekt FMFI 2020/2021

## Description
Convert images into patterns or curves which are then converted to g-code. This g-code can be used by cnc to burn the particular pattern
into some material e.g. wood.

# Available curves and patterns
![original (image from pixabay.com)](examples/christmas.jpg)
original example image (image from pixabay.com)

# Hilbert Curve


![converted using colored Hilbert curve](examples/hilbert_color.png)
converted using colored Hilbert curve

![converted using width of Hilbert curve lines](examples/hilbert_width.png)
converted using width of Hilbert curve lines

![Zoomed in](examples/detail1.png)
Zoomed in

# Gosper curve

![converted using colored Gosper curve](examples/gosper_color.png)
converted using colored Gosper curve

![converted using width of Gosper curve](examples/gosper_width.png)
converted using width of Gosper curve

![Zoomed in](examples/detail3.png)
Zoomed in

# Sierpiński curve

![converted using colored Sierpinski curve](examples/sierpinski_color.png)
converted using colored Sierpinski curve

![converted using width of Sierpiński curve](examples/sierpinski_width.png)
converted using width of Sierpinski curve

![Zoomed in](examples/detail4.png)
Zoomed in

# Spiral

![converted using colored spiral](examples/spiral_color.png)
converted using colored spiral

![converted using width of spiral curve](examples/spiral_width.png)
converted using width of spiral

![Zoomed in](examples/detail5.png)
Zoomed in

# Flower

![converted using colored flower pattern](examples/flower.png)
converted using colored flower pattern

# Wave

![converted using colored wave pattern](examples/wave.png)
converted using colored wave pattern

# Circle mesh

![converted using colored circle mesh pattern](examples/circle_mesh.png)
converted using colored circle mesh pattern

# Partial Circles
![converted using colored circle mesh pattern](examples/partial_circles.png)
converted using colored partial circles pattern

# Weird mesh
![converted using colored circle mesh pattern](examples/weird_mesh.png)
converted using colored mesh pattern

# Area selection

# Draw select
![selected area by drawing on image](examples/area_selection.png)
area can be selected by drawing line on the image

# Contour select
![example image](examples/squirrel.jpg)
example image from pixabay
![example image](examples/squirrel_contour.png)
area can be selected using opencv contour detection