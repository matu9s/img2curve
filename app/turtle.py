import math


class PathTurtle:
    # Turtle graphics, does not draw, returns turtles positions path as list
    def __init__(self):
        self.direction = [0, 1]
        self.position = [0, 0]
        self.pen = False
        self.path = list()

    def forward(self, distance=1.0):
        # moves turtle in direction
        self.position[0] += self.direction[0] * distance
        self.position[1] += self.direction[1] * distance
        if self.pen:
            self.path.append(self.position.copy())

    def turn(self, angle):
        # change turtle's direction
        angle_now = math.atan2(self.direction[1], self.direction[0])
        angle_new = angle_now + math.radians(angle)
        self.direction = [math.cos(angle_new), math.sin(angle_new)]

    def pen_up(self):
        # do not append points on path
        self.pen = False
        self.path.append(None)

    def pen_down(self):
        # append points on path
        self.path.append(None)
        self.pen = True

    def set_position(self, x, y):
        # change position of turtle
        self.position = [x, y]

    def get_path(self):
        # returns list with visited points
        return self.path

    def circle(self, radius=2):
        # draws circle with center set to turtle's position
        prev_pen = self.pen
        prev_position = self.position[:]
        prev_direction = self.direction[:]
        self.direction = [0,1]
        number_of_sides = 180
        inner_angle = (2*math.pi)/number_of_sides
        side_len = 2*radius*math.sin(inner_angle/2)
        self.turn(math.degrees(inner_angle/2))
        self.pen_up()
        self.set_position(self.position[0]+radius, self.position[1])
        self.pen = prev_pen
        for _ in range(0, number_of_sides):
            self.forward(side_len)
            self.turn(math.degrees(inner_angle))
        self.forward(side_len)
        self.turn(math.degrees(inner_angle/2))
        self.pen_up()
        self.position = prev_position[:]
        self.direction = prev_direction[:]
        self.pen = prev_pen

    def partial_circle(self, radius=2, part=180):
        # draws partial circle with center set to turtle's position
        # part: 360 is whole circle, 180 is half circle...
        prev_pen = self.pen
        prev_position = self.position[:]
        prev_direction = self.direction[:]
        self.direction = [-1, 0]
        number_of_sides = 360
        inner_angle = (2*math.pi)/number_of_sides
        side_len = 2*radius*math.sin(inner_angle/2)
        self.turn(math.degrees(inner_angle/2))
        self.pen_up()
        self.set_position(self.position[0], self.position[1]-radius)
        self.pen = prev_pen
        self.pen_up()
        down = False
        for i in range(number_of_sides):
            if i > (number_of_sides-part)//2 and not down:
                self.pen_down()
                down = True
            if i > part+(number_of_sides-part)//2:
                break
            self.forward(side_len)
            self.turn(math.degrees(inner_angle))
        self.forward(side_len)
        self.turn(math.degrees(inner_angle/2))
        self.pen_up()
        self.position = prev_position[:]
        self.direction = prev_direction[:]
        self.pen = prev_pen
