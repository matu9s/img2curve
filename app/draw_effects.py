from abc import ABC, abstractmethod
from PIL import ImageDraw


class Effect(ABC):
    # abstract class for effects
    def __init__(self, input_img, output_img):
        self.output_img = output_img
        self.input_img = input_img

        self.output_draw = ImageDraw.Draw(output_img)
        self.line_width = 1
        self.darkness = 87
        # darkness values 1-99

    @abstractmethod
    def draw_xy(self, x1_coord, y1_coord, x2_coord, y2_coord):
        # draw line from x1_coord, y1_coord to x2_coord, y2_coord using effect
        pass


class LineColorEffect(Effect):
    # draw line using color of input image
    def __init__(self, input_img, output_img):
        super().__init__(input_img, output_img)
        self.input_pixels = input_img.load()

    def draw_xy(self, x1_coord, y1_coord, x2_coord, y2_coord):
        x_middle = (x1_coord + x2_coord) / 2
        y_middle = (y1_coord + y2_coord) / 2

        line_color = self.input_pixels[x_middle, y_middle]
        self.output_draw.line((x1_coord, y1_coord, x2_coord, y2_coord), fill=line_color, width=self.line_width)


class LineWidthEffect(Effect):
    # draw using line width based on darkness of input image converted to black and white image
    def __init__(self, input_img, output_img):
        super().__init__(input_img, output_img)
        self.bw_input_pixels = input_img.convert('LA').load()

    def draw_xy(self, x1_coord, y1_coord, x2_coord, y2_coord):
        x_middle = (x1_coord + x2_coord) / 2
        y_middle = (y1_coord + y2_coord) / 2

        pixel_color = self.bw_input_pixels[x_middle, y_middle][0]

        line_width_factor = int(255 - (self.darkness * 2.55))

        self.output_draw.line((x1_coord, y1_coord, x2_coord, y2_coord), fill='black',
                              width=(255 - pixel_color) // line_width_factor)
