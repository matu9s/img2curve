from PIL import Image
import draw
import draw_effects
import curves
import area_select

c_drawer = draw.CurveDrawer(curves.gosper, 6)
                            # function from curves.py, order of function

input_img = Image.open("christmas.jpg")

c_drawer.prepare_curve(input_img, False, curve_width=10000, curve_height=10000, x_offset=-3000, y_offset=-3000)
                       # PIL Image object, keep aspect ratio

c_drawer.select_area(area_select.DrawSelect)
                    # by drawing line around area, only selected area will be converted
                    # ContourSelect select by finding contours in image

c_drawer.line_width = 5
# change width of drawing line

res = c_drawer.draw(draw_effects.LineColorEffect, 95)
                    # draw effect from draw_effects, darkness

res.save("res.png")

res = c_drawer.to_gcode(input_img.width, input_img.height)
                        # width, height of g-code drawing


with open("test.nc", "w") as output_txt:
    output_txt.write(res)
# write g-code to test.nc file

