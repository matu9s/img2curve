import math
import random


def gosper(turtle, order, symbol="A"):
    # draw Gosper curve using PathTurtle and Lindenmayer system
    if order == 0:
        turtle.forward()
        return
    if symbol == "A":
        lsystem = "A-B--B+A++AA+B-"
    if symbol == "B":
        lsystem = "+A-BB--B-A++A+B"
    for char in lsystem:
        if char == 'A':
            gosper(turtle, order - 1, "A")
        if char == 'B':
            gosper(turtle, order - 1, "B")
        if char == '-':
            turtle.turn(60)
        if char == '+':
            turtle.turn(-60)


def sierpinski(turtle, order, substitution="F--XF--F--XF"):
    # draw Sierpinski curve using PathTurtle and Lindenmayer system
    if order == 0:
        return
    for char in substitution:
        if char == 'F':
            turtle.forward()
        if char == 'G':
            turtle.forward()
        if char == '+':
            turtle.turn(45)
        if char == '-':
            turtle.turn(-45)
        if char == 'X':
            sierpinski(turtle, order - 1, "XF+G+XF--F--XF+G+X")


def _to_hilbert_xy(point_number, order):
    # iterative algorithm for generating points in Hilbert curve
    # returns position of point based on order of curve and index of node
    quadrants = [[0, 0], [0, 1], [1, 1], [1, 0]]
    quadrant = point_number & 3

    x_coord = quadrants[quadrant][0]
    y_coord = quadrants[quadrant][1]

    for i in range(1, order):
        point_number = point_number >> 2
        quadrant = point_number & 3
        distance = 2 ** i
        if quadrant == 0:
            x_coord, y_coord = y_coord, x_coord
        elif quadrant == 1:
            y_coord += distance
        elif quadrant == 2:
            x_coord += distance
            y_coord += distance
        elif quadrant == 3:
            x_coord, y_coord = distance - 1 - y_coord, distance - 1 - x_coord
            x_coord += distance

    return x_coord, y_coord


def hilbert(turtle, order):
    # draw hilbert curve
    number_of_hilbert_points = 2 ** (order * 2)
    points = list()
    for hilbert_point in range(1, number_of_hilbert_points):
        points.append(list(_to_hilbert_xy(hilbert_point, order)))
    turtle.path = points


def spiral(turtle, order):
    # draw spiral using PathTurtle
    j = 0
    for i in range(order * 800):
        j += 0.0001
        for k in range(i // 80):
            turtle.forward(j / (i // 80))
        turtle.turn(10)


def flower(turtle, order):
    # draw flower by overlapping circles order times
    number_of_sides = 6
    inner_angle = (2 * math.pi) / number_of_sides
    radius = 10
    for row in range(order):
        for column in range(order):
            center = [column*radius*4, row*radius*4]
            turtle.set_position(center[0], center[1])
            turtle.direction = [1.0, 0.0].copy()
            turtle.forward(radius)
            turtle.turn(+90)
            turtle.turn(math.degrees(inner_angle / 2))
            for _ in range(0, number_of_sides):
                turtle.pen_up()
                turtle.forward(radius)
                turtle.pen_down()
                turtle.circle(radius)
                turtle.turn(math.degrees(inner_angle))


def wave(turtle, order):
    # draw waves using sine function
    for i in range(order*7):
        turtle.turn(217.55)
        for j in range(1000):
            turtle.forward(7)
            turtle.turn(math.degrees(math.sin(j)))
        turtle.direction = [0, 1]
        turtle.set_position(0, i * 11)
        turtle.forward(21)


def circle_mesh(turtle, order):
    # draw mesh from overlapping circles
    turtle.turn(-90)
    for j in range(order*10):
        for i in range(order*10):
            turtle.circle(40)
            turtle.pen_up()
            turtle.forward(40)
            turtle.pen_down()
        if j % 2 == 0:
            turtle.set_position(0, j * 40)
        else:
            turtle.set_position(40, j * 40)


def weird(turtle, order):
    turtle.pen_down()
    line_len = 50
    for row in range(20*order):
        for column in range(20*order):
            if row % 2 == 0:
                turtle.set_position(math.sqrt(3) * line_len / 2 + (column * math.sqrt(3) * line_len), row * 80)
            else:
                turtle.set_position(column * math.sqrt(3) * line_len, row * 80)
            turtle.direction = [0, 1]
            turtle.pen_down()
            for j in range(7):
                turtle.forward(line_len // 2)
                prev_position = turtle.position.copy()
                prev_direction = turtle.direction.copy()
                turtle.turn(60)
                turtle.forward(line_len / 2)
                turtle.turn(60)
                turtle.forward(line_len / 2)
                turtle.pen_up()
                turtle.position = prev_position.copy()
                turtle.direction = prev_direction.copy()
                turtle.forward()
                turtle.pen_down()
                turtle.forward(line_len // 2)
                turtle.turn(60)
            turtle.pen_up()


def partial_circles(turtle, order):
    turtle.pen_down()
    turtle.turn(180)
    for row in range(order*7):
        for column in range(order*8):
            if row % 2 == 0:
                turtle.set_position(column * 45, row * 25)
            else:
                turtle.set_position(24 + column * 45, row * 25)
            for i in range(7):
                turtle.partial_circle(i * 2 + 10, i * 30)
