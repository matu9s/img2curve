import math
import tkinter as tk
from tkinter import NW
from PIL import ImageTk
from dataclasses import dataclass
import cv2 as cv
import numpy


def compress_area(area):
    new_area = list()
    if len(area) <= 100:
        return area
    else:
        for i in range(0, len(area), (len(area) // 100)+1):
            new_area.append(area[i])
    return new_area


@dataclass
class Point:
    x: float
    y: float


@dataclass
class Line:
    A: Point
    B: Point


def inside_area(point, area):
    # check if point is inside area specified by points using ray casting algorithm
    if not area:
        return True
    point_t = Point(point[0], point[1])
    number_of_intersections = 0
    for i in range(len(area)):

        if i == len(area)-1:
            edge = Line(Point(area[0][0], area[0][1]), Point(area[-1][0], area[-1][1]))
        else:
            edge = Line(Point(area[i][0], area[i][1]), Point(area[i+1][0], area[i+1][1]))

        if point_t.y == edge.A.y or point_t.y == edge.B.y:
            point_t.y += 0.001

        if edge.A.y > edge.B.y:
            edge.A, edge.B = edge.B, edge.A

        if point_t.y > edge.B.y or point_t.y < edge.A.y:
            continue

        if point_t.x > max(edge.A.x, edge.B.x):
            continue

        if edge.B.x - edge.A.x > 0:
            angle_edge = (edge.B.y - edge.A.y) / (edge.B.x-edge.A.x)
        else:
            angle_edge = math.inf

        if point_t.x - edge.A.x > 0:
            angle_point = (point_t.y - edge.A.y) / (point_t.x - edge.A.x)
        else:
            angle_point = math.inf

        if angle_point >= angle_edge:
            number_of_intersections += 1

    if number_of_intersections % 2 == 0:
        return False
    else:
        return True


def extreme_points(path):
    max_x = -1
    min_x = math.inf
    max_y = -1
    min_y = math.inf
    for point in path:
        max_x = max(max_x, point[0])
        min_x = min(min_x, point[0])
        max_y = max(max_y, point[1])
        min_y = min(min_y, point[1])
    return {"min_x": min_x, "max_x": max_x, "min_y": min_y, "max_y": max_y}


class DrawSelect:
    def __init__(self, input_img):
        self.img = input_img
        self.img = self.img.resize((900, int(self.img.height * (900 / self.img.width))))
        self.x_scaling = input_img.width / 900
        self.y_scaling = input_img.height / 600
        self.root = tk.Tk()
        self.root.title("Area Selection")
        self.canvas = tk.Canvas(self.root, width=900, height=600)
        self.tk_img = ImageTk.PhotoImage(self.img)
        self.canvas.pack()
        self.canvas.create_image(0, 0, anchor=NW, image=self.tk_img)
        self.prev_coords = None
        self.points = list()
        self.root.bind('<B1-Motion>', self._mouse_move)
        self.root.bind("<ButtonRelease-1>", self._mouse_release)
        self.root.mainloop()

    def _mouse_move(self, event):
        self.points.append([event.x*self.x_scaling, event.y*self.y_scaling])
        if self.prev_coords:
            self.canvas.create_line(self.prev_coords[0], self.prev_coords[1], event.x, event.y)
        self.prev_coords = (event.x, event.y)

    def _mouse_release(self, event):
        self.root.destroy()

    def get_area(self):
        return self.points


class ContourSelect:
    def __init__(self, input_img):
        self.img = cv.imread(input_img.filename)
        self.img = cv.cvtColor(self.img, cv.COLOR_BGR2GRAY)

    def get_area(self):
        ret, thresh = cv.threshold(self.img, 120, 255, cv.THRESH_BINARY)
        contours, hierarchy = cv.findContours(image=thresh, mode=cv.RETR_EXTERNAL, method=cv.CHAIN_APPROX_NONE)
        contours = sorted(contours, key=cv.contourArea, reverse=True)
        contour = contours[0]
        epsilon = 0.000001 * cv.arcLength(contour, True)
        approx = cv.approxPolyDP(contour, epsilon, True)
        approx_list = numpy.ndarray.tolist(approx)
        res_list = []
        for coord in approx_list:
            res_list.append([coord[0][0], coord[0][1]])
        return res_list
