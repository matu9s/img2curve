import math

from PIL import Image
import pygcode
from turtle import PathTurtle
import area_select


class CurveDrawer:
    def __init__(self, curve_function, order):
        self.curve_function = curve_function
        self.order = order
        self.input_img = None
        self.path = list()
        self.input_img_width = 0
        self.input_img_height = 0
        self.background_color = 'white'
        self.line_width = 1
        self.curve_bottom = 0
        self.curve_top = 0
        self.curve_left = 0
        self.curve_right = 0
        self.selected_area = []

    def _adjust_path(self, width, height, keep_aspect_ratio=False, x_offset=0, y_offset=0):
        # adjust point path to specified width and height
        max_x = -math.inf
        min_x = math.inf

        max_y = -math.inf
        min_y = math.inf

        for point in self.path:
            if point is not None:
                max_x = max(max_x, point[0])
                min_x = min(min_x, point[0])

            if point is not None:
                max_y = max(max_y, point[1])
                min_y = min(min_y, point[1])

        curve_width = abs(max_x - min_x)
        curve_height = abs(max_y - min_y)

        x_adjust = min_x
        y_adjust = min_y

        for i in range(len(self.path)):
            if self.path[i] is not None:
                self.path[i][0] -= x_adjust
                self.path[i][1] -= y_adjust

        if keep_aspect_ratio:
            smaller_dimension = min(width, height)
            width_factor = (smaller_dimension - 1) / curve_width
            height_factor = (smaller_dimension - 1) / curve_height
        else:
            width_factor = (width - 1) / curve_width
            height_factor = (height - 1) / curve_height

        for i in range(1, len(self.path)):
            if self.path[i] is not None:
                self.path[i][0] *= width_factor
                self.path[i][0] += x_offset

                self.path[i][1] *= height_factor
                self.path[i][1] += y_offset

                self.curve_left = min(self.curve_left, self.path[i][0])
                self.curve_right = max(self.curve_right, self.path[i][0])

                self.curve_top = min(self.curve_top, self.path[i][1])
                self.curve_bottom = max(self.curve_bottom, self.path[i][1])

    def prepare_curve(self, input_img, keep_aspect_ratio=False, **kwargs):
        # compute points of curve
        self.input_img = input_img
        self.input_img_width = input_img.width
        self.input_img_height = input_img.height

        path_turtle = PathTurtle()

        path_turtle.pen_down()
        self.curve_function(path_turtle, self.order)
        self.path = path_turtle.get_path()
        self._adjust_path(kwargs.get("curve_width", self.input_img.width),
                          kwargs.get("curve_height", self.input_img_height),
                          keep_aspect_ratio, kwargs.get("x_offset", 0), kwargs.get("y_offset", 0))

    def select_area(self, area_select_type):
        area_selector = area_select_type(self.input_img)
        self.selected_area = area_select.compress_area(area_selector.get_area())

    def draw(self, draw_effect_type, darkness=87):
        # draw image using effect type from draw_effects.py, darkness available in line width effect
        output_img = Image.new('RGB', (self.input_img_width, self.input_img_height), color=self.background_color)

        draw_effect = draw_effect_type(self.input_img, output_img)
        draw_effect.darkness = darkness
        draw_effect.line_width = self.line_width

        for i in range(2, len(self.path)):
            if self.path[i] is not None and self.path[i-1] is not None:
                x1_coord = self.path[i - 1][0]
                y1_coord = self.path[i - 1][1]
                x2_coord = self.path[i][0]
                y2_coord = self.path[i][1]

                if (0 < x1_coord < self.input_img_width and 0 < x2_coord < self.input_img_width
                        and 0 < y1_coord < self.input_img_height and 0 < y2_coord < self.input_img_height
                        and area_select.inside_area([x1_coord, y1_coord], self.selected_area)
                        and area_select.inside_area([x2_coord, y2_coord], self.selected_area)):

                    draw_effect.draw_xy(x1_coord, y1_coord, x2_coord, y2_coord)

        return draw_effect.output_img

    def to_gcode(self, output_width, output_height):
        # returns string with g-code
        gcode = [
            pygcode.GCodeRapidMove(Z=5),
            pygcode.GCodeStartSpindleCW()]
        bw_input_img = self.input_img.convert('LA')
        bw_input_pixels = bw_input_img.load()

        curve_width = self.curve_right - self.curve_left
        curve_height = self.curve_bottom - self.curve_top

        for i in range(2, len(self.path)):
            if self.path[i] is not None and self.path[i-1] is not None:
                x1_coord = self.path[i - 1][0]
                y1_coord = self.path[i - 1][1]
                x2_coord = self.path[i][0]
                y2_coord = self.path[i][1]

                x_middle = (x1_coord + x2_coord) / 2
                y_middle = (y1_coord + y2_coord) / 2

                img_bottom = min(self.input_img_height, self.curve_bottom)

                if (0 < x1_coord < self.input_img_width and 0 < x2_coord < self.input_img_width
                        and 0 < y1_coord < self.input_img_height and 0 < y2_coord < self.input_img_height
                        and 0 < img_bottom - y_middle - 1 < self.input_img_height
                        and area_select.inside_area([x1_coord, img_bottom - y1_coord], self.selected_area)
                        and area_select.inside_area([x2_coord, img_bottom - y2_coord], self.selected_area)):
                    pixel_color = bw_input_pixels[x_middle, img_bottom - y_middle - 1][0]
                    gcode.append(pygcode.GCodeRapidMove(X=(x1_coord / curve_width) * output_width,
                                                        Y=(y1_coord / curve_height) * output_height))
                    gcode.append(pygcode.GCodeSpindleSpeed((255 - pixel_color) * 4))
                    gcode.append(pygcode.GCodeRapidMove(X=(x2_coord / curve_width) * output_width,
                                                        Y=(y2_coord / curve_height) * output_height))
                    gcode.append(pygcode.GCodeSpindleSpeed(0))

        output_gcode = ""
        for code in gcode:
            output_gcode += str(code) + "\n"

        return output_gcode
